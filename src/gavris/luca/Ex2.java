package gavris.luca;

public class Ex2 {
    public static void main(String[] args) {
        testThread a = new testThread("NiceThread1");
        testThread b = new testThread("NiceThread2");
        testThread c = new testThread("NiceThread3");
        a.start();
        b.start();
        c.start();
    }
}

class testThread extends Thread{
    private String name;

    public testThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 13; i++){

            System.out.println(this.name + " " + i);
            try {
                Thread.sleep(2000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}